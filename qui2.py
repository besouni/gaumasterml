print("Quiz 2")
#===Part _ 1===
# import random
# import pandas as pd
# import numpy as np
# N = 10
# DAYS = 7
# students_id = random.sample(range(10000, 99999), N)
# # print(students_id)
# data = {
#     'students_id': students_id
# }
# days = []
# for i in range(0, DAYS):
#     day = np.round(np.random.uniform(0, 5, size=N), 2)
#     data_index = 'day'+str(i+1)
#     days.append(data_index)
#     data[data_index] = day
# df = pd.DataFrame(data)
# print(df)
# print(days)
# random_rows = random.sample(range(0, N), int(N*0.2))
# # print(random_rows)
# # print(df.loc[[random_rows[0], random_rows[1]]])
# for i in random_rows:
#     random_index_in_rows = random.sample(range(0, len(days)), int(len(days) * 0.5))
#     # print(i)
#     # print(random_index_in_rows)
#     for j in random_index_in_rows:
#         df.at[i, days[j]] = None
# print(df)
# df.to_excel("q2/data_test.xls", index=False)


print("Quiz 2")
#===Part _ 2===
import pandas as pd
import matplotlib.pyplot as plt
import random
import numpy as np
from sklearn import linear_model

N = 9
# ---3---
# df =  pd.read_excel("q2/data_test.xls")
# print(df)
# days = list(df.columns)[1:]
# print(days)
# exercises_sum = []
# for item in days:
#     exercises_sum.append(round(df[item].sum(), 0))
# # print(exercises_sum)
# Y = exercises_sum
# X = [*range(1, 8)]
# # print(X)
# plt.plot(X, Y)
# plt.scatter(X, Y, marker='x', color='red')
# plt.show()
# print("======================")

# ---4---
# df =  pd.read_excel("q2/data_test.xls")
# print(df)
# random_data = df.loc[[random.randint(0, N)]].transpose()
# print(random_data)
# random_data = random_data[1:]
# print(random_data.columns)
# random_data.rename(columns={random_data.columns.values[0]:'time'}, inplace=True)
# print(random_data)
# print(random_data.time.mean())
# print(random_data.time.median())
# print(random_data.time.std())
# print(random_data.time.max())
# print(random_data.time.min())
# print("======================")

# ---5---
# df =  pd.read_excel("q2/data_test.xls")
# # print(df)
# collumns = list(df)
# # print(collumns)
# collumns.remove('students_id')
# # print(collumns)
# df['sum'] = df[collumns].sum(axis=1)
# # print(df)
# houres = df['sum']
# print(np.array(houres))
# print(np.percentile(np.array(houres), 50))

# ---6---
df =  pd.read_excel("q2/data.xls")
collumns = list(df)
collumns.remove('students_id')
df['sum'] = df[collumns].sum(axis=1)
houres = df['sum']
# print(houres)
houres = np.array(houres)
# print(houres)
houres_r = houres.reshape(20, 5)
print(houres_r)
y = []
X = []
# print(round(houres[:5].sum(), 2))
# print(houres_r[0].sum())
for i in range(5, 101, 5):
    # print(round(houres[:i].sum(), 2))
    y.append(round(houres[:i].sum(), 2))
    X.append(i)
# print(houres.sum())
print(X)
print(y)
plt.scatter(X, y)
plt.plot(X, y)
plt.show()
model = linear_model.LinearRegression()
X = np.array(X)
X = X.reshape(20, 1)
print(X)
model.fit(X, y)
print(model.score(X, y))
predicted_values = [[105,], [110,], [115,], [117,]]
print(np.round(model.predict(predicted_values), 2))

