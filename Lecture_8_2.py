from scrapy.http import TextResponse
import requests
import pprint
import pandas as pd

url = requests.get("https://ss.ge/ka/udzravi-qoneba/l/bina/iyideba?MunicipalityId=95&CityIdList=95&StatusField.FieldId=34&StatusField.Type=SingleSelect&StatusField.StandardField=Status&PriceType=false&CurrencyId=1")
response = TextResponse(url.url, body=url.text, encoding='utf-8')
# print(response)
items = response.css("div.latest_desc div a::attr(href)").getall()
# pprint.pprint(items)

counter = 0

area = []
rooms = []
bedrooms = []
price = []

for item in items:
    # print(item)
    if item != "/ka/home/promopaidservices" and item != "/ka/home/agent":
        print(item)
        url_for_itmes = requests.get("https://ss.ge"+item)
        response_for_items = TextResponse(url_for_itmes.url, body=url_for_itmes.text, encoding='utf-8')
        item_info =  response_for_items.css("div.ParamsHdBlk text::text").extract()
        item_price = response_for_items.css("div.article_right_price::text").extract();
        if(len(item_info)!=0):
            counter += 1
            print(item_info)
            print(item_price[0].strip())
            area.append(item_info[0][:-2])
            rooms.append(item_info[1])
            bedrooms.append(item_info[2])
            price.append(item_price[0].strip())
            if counter==5:
                break
data = {
    "area":area,
    "rooms":rooms,
    "bedrooms":bedrooms,
    "price":price
}
df = pd.DataFrame(data)
df.to_excel("test_data/ss.xlsx", index=False)

