import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

x = [5,7,8,7,2,17,2,9,4,11,12,9,6]
y = [99,86,87,88,111,86,103,87,94,78,77,85,86]

slope, intercept, r, p, std_err = stats.linregress(x, y)



print(r)
print(slope)
print(intercept)


def myfunc(x):
  return slope * x + intercept

mymodel = list(map(myfunc, x))

print(myfunc(16))
print(myfunc(22))

plt.scatter(x, y)
plt.plot(x, mymodel)
plt.show()





#plot 1:
# x = np.array([0, 1, 2, 3])
# y = np.array([3, 8, 1, 10])
#
# plt.subplot(2, 1, 1)
# plt.plot(x,y)
#
# #plot 2:
# x = np.array([0, 1, 2, 3])
# y = np.array([10, 20, 30, 40])
#
# plt.subplot(2, 1, 2)
# plt.plot(x,y)
#
# plt.show()

# plt.title("matplotlib")
# plt.xlabel("X Label")
# plt.ylabel("Y Label")
# xpoints = np.array([0, 4, 6])
# ypoints = np.array([0, 80, 250])

# plt.plot(xpoints, ypoints, 's:r')
# # plt.scatter(xpoints, ypoints, marker="x")
# plt.show()