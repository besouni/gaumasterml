#===Task 1===
# import math
# r = []
# for n in range(14, 100, 7):
#     counter = 0
#     for i in range(2, int(math.sqrt(n))):
#         if n%i==0:
#             counter += 2
#     if counter==2:
#         r.append(n)
# print(r)
#===Task 3===
# import string
# s = "hello 87 my 65  world"
# print(string.digits)
# print(string.ascii_letters)
#===Task 4====
import pandas as pd
import random
from datetime import date
import numpy as np
N = 100

start_date = date(2019, 1, 1)
end_date = date(2019, 2, 1)
date_array = []
for _ in range(0, N):
    date_array.append(start_date+(end_date-start_date)*random.random())

Hourly_Wage = random.sample(range(10, 100), int(N*0.10))
for i in range(0, int(N*0.90)):
    Hourly_Wage.insert(random.randint(0, len(Hourly_Wage)), "NAN")

data = {
    "ID": random.sample(range(1000, 10000), N),
    "Date":date_array,
    "Time":np.random.randint(1, 9, N),
    "Hourly_Wage":Hourly_Wage
}

df = pd.DataFrame(data)
df.loc[random.sample(range(0, 100), 5), "ID"] = "NAN"
df.loc[random.sample(range(0, 100), 7), "Date"] = "NAN"
df.loc[random.sample(range(0, 100), int(N*0.3)), "Time"] = "NAN"
df.to_excel("data.xlsx", sheet_name="sheetOne")


