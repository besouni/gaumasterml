import pandas as pd
import os.path
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

if not os.path.exists("test_data/gdp_for_georgia.xls"):
    PATCH_DATA = "test_data/gdp_georgia_per_capita.xls"
    data = pd.read_excel(PATCH_DATA, sheet_name="Data", header=3)
    # print(data)
    gdp_for_geo =  data[data["Country Name"]=="Georgia"]
    # print(gdp_for_geo)
    data_for_geo = pd.DataFrame(gdp_for_geo.transpose())
    data_for_geo.to_excel("test_data/gdp_for_georgia.xls")
data =  pd.read_excel("test_data/gdp_for_georgia.xls", skiprows=4)
# print(data)
data.rename(columns={"Indicator Code": "Year", "NY.GDP.PCAP.CD": "GDP"}, inplace=True)
# print(data)
data.dropna(subset=["GDP"], inplace=True)
# print(data)

plt.scatter(data.Year, data.GDP, color="red", s=50, marker='x')
plt.plot(data.Year, data.GDP)
plt.xlabel("Year")
plt.ylabel("GDP")
plt.show()

print(f"Mean = {data.GDP.mean()}")
print(f"Median = {data.GDP.median()}")
print(f"std = {round(data.GDP.std(), 2)}")

np_data = np.array(data.GDP)
print(f"50 Percentile = {np.percentile(np_data, 50)}")

model = linear_model.LinearRegression()
X = data[['Year']]
y = data.GDP
model.fit(X,y)
print(f"Score = {model.score(X,y)}")
print(model.coef_)
print(model.intercept_)
predict_data = np.array([[2021], [2022], [2023], [2024]])
print(np.round(model.predict(predict_data), 2))